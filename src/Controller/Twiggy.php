<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Csrf\TokenGenerator\UriSafeTokenGenerator;
use Symfony\Component\Security\Csrf\TokenStorage\SessionTokenStorage;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Symfony\Component\Form\Extension\Csrf\CsrfExtension as SymfonyCsrfExtension;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use Twig\RuntimeLoader\FactoryRuntimeLoader;
use Symfony\Component\Form\FormRenderer;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\XliffFileLoader;
use Symfony\Bridge\Twig\Extension\TranslationExtension;

class Twiggy 
{
    public function index(): Response
    {
        return $this->buildForm();
    }

    public function buildForm(): Response
    {
            $session = new Session();
            $csrfGenerator = new UriSafeTokenGenerator();
            $csrfStorage = new SessionTokenStorage($session);
            $csrfManager = new CsrfTokenManager($csrfGenerator, $csrfStorage);

            //$defaultFormTheme = 'form_div_layout.html.twig';
            $defaultFormTheme = 'bootstrap_4_horizontal_layout.html.twig';

            // the path to TwigBridge library so Twig can locate the
            // form_div_layout.html.twig file
            $appVariableReflection = new \ReflectionClass('\Symfony\Bridge\Twig\AppVariable');
            $vendorTwigBridgeDirectory = dirname($appVariableReflection->getFileName());
            // the path to your other templates
            $viewsDirectory = dirname(__DIR__,2) .'/views';

            $translator = new Translator('en');
            $translator->addLoader('xlf', new XliffFileLoader());
            $translator->addResource(
                'xlf',
                dirname(__DIR__,2).'/translations/messages.en.xlf',
                'en'
            );
            
            $twig = new Environment(new FilesystemLoader([
                $viewsDirectory,
                $vendorTwigBridgeDirectory.'/Resources/views/Form',
            ]));

            $formEngine = new TwigRendererEngine([$defaultFormTheme], $twig);
            $twig->addRuntimeLoader(new FactoryRuntimeLoader([
                    FormRenderer::class => function () use ($formEngine, $csrfManager) {
                        return new FormRenderer($formEngine, $csrfManager);
                    },
                ])
            );
            // adds the FormExtension to Twig
            $twig->addExtension(new TranslationExtension($translator));
            $twig->addExtension(new FormExtension());

            $formFactory = Forms::createFormFactoryBuilder()
            ->addExtension(new HttpFoundationExtension())
            ->addExtension(new SymfonyCsrfExtension($csrfManager))
            ->getFormFactory();

            $form = $formFactory->createBuilder()
            ->add('task', TextType::class)
            ->add('dueDate', DateType::class)
            ->getForm();

            return new Response($twig->render('new.html.twig', [
                'form' => $form->createView(),
            ]));

    }

}